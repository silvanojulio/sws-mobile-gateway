import express, { Application }         from 'express'
import { Response, Request }            from 'express'
import axios, { AxiosRequestConfig }    from 'axios'
import https    from 'https'
import fs       from 'fs'
import cors     from 'cors'
import { exit } from 'process'

const cert_key=fs.existsSync('certs/gateway.sistemaws.com-key.pem')?fs.readFileSync('certs/gateway.sistemaws.com-key.pem','utf8'):null
const cert_crt=fs.existsSync('certs/gateway.sistemaws.com-crt.pem')?fs.readFileSync('certs/gateway.sistemaws.com-crt.pem','utf8'):null
if(!cert_key || !cert_crt) {
    console.log("faltan los certificados en la carpeta certs")
    exit()
}
const credentials = {key: cert_key, cert: cert_crt};
const app = express()
const port = 3001

const gateway = async (request:Request, response:Response):Promise<Application> =>{
    const url=request.params[0]
    const CURRENTTOKENVALUE=request.headers['currenttokenvalue']
    try{
        const config = {
            url,
            method:request.method,
            params:request.query,
            data:request.body
        } as AxiosRequestConfig
        if(!!CURRENTTOKENVALUE) config.headers={CURRENTTOKENVALUE}
        const res=await axios(config)
        response.send(res.data)
    }catch(error){
        response.status(500).send(error)
    }
    return request.app
}

app.use(cors())
.use(express.json())
.all('/*',gateway)

const httpsServer=https.createServer(credentials,app)
httpsServer.listen(port)